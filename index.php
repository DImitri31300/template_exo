<?php
include 'profil.php';

// CONFIG FAKER
require_once 'vendor/autoload.php';
$faker = Faker\Factory::create();

// CONFIGURATION DU TEMPLATE
$loader = new Twig_Loader_Filesystem('templates'); // Dossier contenant les templates
$twig = new Twig_Environment($loader, array(
  'cache' => false // __DIR__ . "/tmp"
));



// ROUTING
$page = 'profil';
if(isset($_GET['p'])) {
    $page = $_GET['p'];
}

// ROAD
switch($page) {
    case 'profil':
      echo $twig->render('profil.twig',['title' =>  $faker->company, 'title2'=> $faker->catchPhrase, 'color' => $faker->hexColor, 'text'=> $faker->text, 'emoji' => $faker->emoji, 'img' => $faker->imageUrl($width = 200, $height = 250, 'people')]);
      break;
    default:
      header('HTTP/1.8 404 Not found');
      echo $twig->render('404.twig');
      break;
  }

?>